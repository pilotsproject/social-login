//
//  SocialSignIn.swift
//  GoogleDemo
//
//  Created by omima ibrahim on 11/10/16.
//  Copyright © 2016 omima.ibrahim. All rights reserved.
//

import Foundation
import GoogleSignIn
import Google
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import TwitterKit

class SocialSignIn : NSObject , GIDSignInDelegate , GIDSignInUIDelegate {
    
    var viewCont : ViewController!
    
    var dict : [String : AnyObject]!
    
    init(viewCont : ViewController ) {
        self.viewCont = viewCont
    }
    
    func twitterSetup(btn : UIButton) {
        btn.addTarget(self, action:#selector(twitterAtion), for: .touchUpInside)

    }
    
   
    @objc func twitterAtion(_ sender: UIButton){
        Twitter.sharedInstance().logIn { session, error in
            if (session != nil) {
                let alert = UIAlertController(title: "Logged In",
                                              message: "User \(session!.userName) has logged in",
                    preferredStyle: UIAlertControllerStyle.alert
                )
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.viewCont.present(alert, animated: true, completion: nil)
                
                print(session!.userName)
            } else {
                NSLog("Login error: %@", error!.localizedDescription)
            }
        }
    }
    
    
    
    func facebookSetup(btn : UIButton)  {
        btn.addTarget(self, action:#selector(loginButtonFacebookClicked), for: .touchUpInside)
    }
    
    @objc func loginButtonFacebookClicked(_ sender: UIButton) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        fbLoginManager.logOut()
        
        fbLoginManager.logIn(withReadPermissions: ["email"], from: viewCont) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                      self.getFBUserData()
                    }
                }
            }
        }
        
    }
    func getFBUserData(){
        
        var dic :[String : AnyObject]!
        
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    dic = result as! [String : AnyObject]
                 
                    print(dic)
    
                }
            })
        }
        
    }
    
    
    func setupGoogleSignIn(_ btn : UIButton )  {
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        btn.addTarget(self, action: #selector(signInGoogleAction), for: .touchUpInside)
    }
    
    @objc func signInGoogleAction(_ sender: UIButton) {
        
        GIDSignIn.sharedInstance().signIn()
    }
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        viewCont.present(viewController, animated: true) { () -> Void in
        }
    }
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        viewCont.dismiss(animated: true) { () -> Void in
        }
    }
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            print("userId",userId , "user name" , (fullName), "idToken",(idToken), "email",(email))
        } else {
            print("\(error.localizedDescription)")
        }
        
    }
    
}
