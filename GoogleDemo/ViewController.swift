//
//  ViewController.swift
//  GoogleDemo
//
//  Created by omima ibrahim on 11/7/16.
//  Copyright © 2016 omima.ibrahim. All rights reserved.
//

import UIKit
import GoogleSignIn
import Google
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import TwitterKit

class ViewController: UIViewController   {

    var socialSignIn : SocialSignIn!
    
    @IBOutlet var facebook: UIButton!
    @IBOutlet var googleBtn: UIButton!
    @IBOutlet var twiiter: UIButton!
    
    var dic : [String : AnyObject]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        socialSignIn = SocialSignIn(viewCont: self)
        socialSignIn.setupGoogleSignIn(googleBtn)
        socialSignIn.facebookSetup(btn: facebook)
        socialSignIn.twitterSetup(btn: twiiter)
        
    }


}



